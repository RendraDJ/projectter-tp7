package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.repositories.SubjectRepository;

import java.util.Optional;

@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    public Iterable<Subject> getSubjects(){
        return subjectRepository.findAll();
    }

    public Subject saveSubject(Subject subject){
        Subject savedSubject = subjectRepository.save(subject);
        return savedSubject;
    }

    public void deleteSubject(final Long id){
        subjectRepository.deleteById(id);
    }

    public Optional<Subject> findById(Long id){
        return subjectRepository.findById(id);
    }


}
