package seleniumTests;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddTeacherTest extends BaseForTests{

    @Test
    void addTeacherByTeacher(){
        login("Lovelace", "lovelace");

        driver.get(serverAdress+"/listTeachers");
        WebElement tbodyBefore = driver.findElement(By.tagName("body"));
        Dimension tbodySizeBefore = tbodyBefore.getSize();

        driver.get(serverAdress+"/addTeacher");
        assertEquals(driver.getTitle(), "Error");

        driver.get(serverAdress+"/");
        driver.get(serverAdress+"/listTeachers");
        WebElement tbodyAfter = driver.findElement(By.tagName("body"));
        Dimension tbodySizeAfter = tbodyAfter.getSize();
        assertEquals(tbodySizeBefore, tbodySizeAfter);

        driver.close();
    }

    @Test
    void addTeacherByTERManager() throws IOException {
        login("Chef", "mdp");

        driver.get(serverAdress+"/addTeacher");
        WebElement firstName = driver.findElement(By.id("firstName"));
        WebElement lastName = driver.findElement(By.id("lastName"));
        WebElement loginButton = driver.findElement(By.cssSelector("[type=submit]"));
        write(firstName, "New Prof");
        write(lastName, "Last Name New Prof");
        click(loginButton);

        driver.get(serverAdress+"/");
        driver.get(serverAdress+"/listTeachers");
        WebElement tbodyAfter = driver.findElement(By.tagName("body"));
        assertTrue(tbodyAfter.getText().contains("New Prof"));
        assertTrue(tbodyAfter.getText().contains("Last Name New Prof"));

        screenshot("photoAfterAddTeacher");

        driver.close();
    }

}
